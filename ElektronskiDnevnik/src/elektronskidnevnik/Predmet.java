/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elektronskidnevnik;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author John O'Ra
 */
public class Predmet {
    
    public void funkcijaPredmeti(char operation, Integer id_predmeta, String predmet){
       
        Connection con = MyConnection.getConnection();
        PreparedStatement ps;
        
        
        if(operation == 'i')// i za insert
        {
            try {
                ps = con.prepareStatement("INSERT INTO predmet(predmet) VALUES (?)");
                ps.setString(1, predmet);

               
                if(ps.executeUpdate() > 0){
                    JOptionPane.showMessageDialog(null, "Dodat je novi predmet.");
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(Korisnik.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if(operation == 'u')// u za update
        {
            try {
                ps = con.prepareStatement("UPDATE `predmet` SET `predmet`= ?, WHERE `id_predmeta` = ?");
                ps.setInt(1, id_predmeta);
                ps.setString(2, predmet);

                if(ps.executeUpdate() > 0){
                    JOptionPane.showMessageDialog(null, "Podaci su azurirani.");
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(Korisnik.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if(operation == 'd')// d za delete
        {
            int DaIliNe = JOptionPane.showConfirmDialog(null, "Predmet ce biti obrisan.","Obrisi",JOptionPane.OK_CANCEL_OPTION,0);
            
            if(DaIliNe == JOptionPane.OK_OPTION)
            {
               try {
                ps = con.prepareStatement("DELETE FROM `predemet` WHERE `id_predmeta` = ?");
                ps.setInt(1,id_predmeta);
                if(ps.executeUpdate() > 0){
                    JOptionPane.showMessageDialog(null, "Predmet je obrisan!");
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(Korisnik.class.getName()).log(Level.SEVERE, null, ex);
            } 
            }
            
            
        }
        
    }
    
    public boolean predmetPostoji(String predmet){
        boolean postoji = false;
        Connection con = MyConnection.getConnection();
        PreparedStatement ps;
        try {
            ps = con.prepareStatement("SELECT * FROM `predmet` WHERE `predmet` = ?");
            ps.setString(1, predmet);
            
            ResultSet rs = ps.executeQuery();
 
            
            if(rs.next()){
                postoji = true;
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Korisnik.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return postoji;
    }
    
    
        public void popuniTabeluPredmeta(JTable table){
        Connection con = MyConnection.getConnection();
        PreparedStatement ps;
        try {
            ps = con.prepareStatement("SELECT * FROM `predmet`");
           
            ResultSet rs = ps.executeQuery();
            DefaultTableModel model = (DefaultTableModel)table.getModel();
            
            Object[] row;
            
            while(rs.next()){
                row = new Object[2];
                row[0] = rs.getInt(1);
                row[1] = rs.getString(2);

                model.addRow(row);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Korisnik.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    private int getIdPredmeta(String predmet){
        int id_predmeta =  0;
        Connection con = MyConnection.getConnection();
        PreparedStatement ps;
        try {
            ps = con.prepareStatement("SELECT * FROM `predmet` WHERE `predmet` = ?");
            ps.setString(1, predmet);
            
            ResultSet rs = ps.executeQuery();
 
            
            if(rs.next()){
                id_predmeta = rs.getInt("id_predmeta");
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Korisnik.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return id_predmeta;
    }
    
            public void comboBoxPredmeti(JComboBox combo){
        Connection con = MyConnection.getConnection();
        PreparedStatement ps;
        try {
            ps = con.prepareStatement("SELECT * FROM `predmet`");
           
            ResultSet rs = ps.executeQuery();

            
            while(rs.next()){
               combo.addItem(rs.getString(2));
               
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Korisnik.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
