/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elektronskidnevnik;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author John O'Ra
 */
public class Zapis {
    
     public void funkcijaPredmeti(char operation, int id_korisnika, int id_predmeta, String ocena, String opis){
       
        Connection con = MyConnection.getConnection();
        PreparedStatement ps;
        
        
        if(operation == 'i')// i za insert
        {
            try {
                ps = con.prepareStatement("INSERT INTO `zapis`(`id_korisnika`, `id_predmeta`, `ocena`, `opis`) VALUES (?,?,?,?)");
                ps.setInt(1, id_korisnika);
                ps.setInt(2, id_predmeta);
                ps.setString(3, ocena);
                ps.setString(4, opis);
               
                if(ps.executeUpdate() > 0){
                    JOptionPane.showMessageDialog(null, "Zapis dodat.");
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(Korisnik.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
            
    }
    
}
