
package elektronskidnevnik;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


public class Korisnik {
    
    private int id_korisnika;
    private String ime;
    private String prezime;
    private String email;
    private String prava_pristupa;
    private String sifra;
    private int id_odeljenja;
    

 public void kontrolaKorisnika(char operation, Integer id, String ime, String prezime, 
                                          String email, String prava_pristupa, String sifra, int id_odeljenja)
    {
       
        Connection con = MyConnection.getConnection();
        PreparedStatement ps;
        
        
        if(operation == 'i')// i za insert
        {
            try {
                ps = con.prepareStatement("INSERT INTO korisnik(ime, prezime, email, prava_pristupa, sifra, id_odeljenja) VALUES (?,?,?,?,?,?)");
                ps.setString(1, ime);
                ps.setString(2, prezime);
                ps.setString(3, email);
                ps.setString(4, prava_pristupa);
                ps.setString(5, sifra);
                ps.setInt(6, id_odeljenja);
               
                if(ps.executeUpdate() > 0){
                    JOptionPane.showMessageDialog(null, "Dodat je novi ucenik.");
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(Korisnik.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if(operation == 'u')// u za update
        {
            try {
                ps = con.prepareStatement("UPDATE `korisnik` SET `ime`= ?, `prezime`= ?, `email`= ?, `prava_pristupa`= ?, `sifra`= ?, `id_odeljenja`= ? WHERE `id_korisnika` = ?");
                ps.setString(1, ime);
                ps.setString(2, prezime);
                ps.setString(3, email);
                ps.setString(4, prava_pristupa);
                ps.setString(5, sifra);
                ps.setInt(6, id_odeljenja);
                ps.setInt(7,id_korisnika);
                if(ps.executeUpdate() > 0){
                    JOptionPane.showMessageDialog(null, "Podaci su azurirani.");
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(Korisnik.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if(operation == 'd')// d za delete
        {
            int DaIliNe = JOptionPane.showConfirmDialog(null, "Bice obrisane sve informacije o korisniku.","Obrisi",JOptionPane.OK_CANCEL_OPTION,0);
            
            if(DaIliNe == JOptionPane.OK_OPTION)
            {
               try {
                ps = con.prepareStatement("DELETE FROM `korisnik` WHERE `id_korisnika` = ?");
                ps.setInt(1,id);
                if(ps.executeUpdate() > 0){
                    JOptionPane.showMessageDialog(null, "Korisnik je obrisan!");
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(Korisnik.class.getName()).log(Level.SEVERE, null, ex);
            } 
            }
            
            
        }
        
    }
    
    public void popuniTabeluKorisnika(JTable table, String valueToSearch){
        Connection con = MyConnection.getConnection();
        PreparedStatement ps;
        try {
            ps = con.prepareStatement("SELECT * FROM `korisnik` WHERE(`ime`, `prezime`, `email`, `id_odeljenja`)LIKE ?");
            ps.setString(1, "%"+valueToSearch+"%");
            
            ResultSet rs = ps.executeQuery();
            DefaultTableModel model = (DefaultTableModel)table.getModel();
            
            Object[] row;
            
            while(rs.next()){
                row = new Object[7];
                row[0] = rs.getInt(1);
                row[1] = rs.getString(2);
                row[2] = rs.getString(3);
                row[3] = rs.getString(4);
                row[4] = rs.getString(5);
                row[5] = rs.getString(6);
                row[6] = rs.getInt(7);
                
                model.addRow(row);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Korisnik.class.getName()).log(Level.SEVERE, null, ex);
        }
    }



}